Feature: User Management

  User Story:
  In order to manage users
  as an administrator
  I need CRUD permission to user accounts

  Rules:

  - Non-admin users have no permission over other accounts
  -There are two types of non-admin accounts
  -- risk managers
  -- asset managers
  - Non admin users mst only be able to see risks or asets in their groups
  - the administrator must be able to reset users passwords
  - when an account is created a us must be forced to reset their password on first logon

  Questions:

  -Do Administrator users need access to all accounts or only to the accounts in their group?

  To Do:
  -Force reset of password on account creation

  Domain language:
  grou = Users are defined by which group they belong to
  CRUD = Create,read,update , delete
  administrator permissions = access to CRUD risks, users and assets for all groups
  risk_management permission = only able to CRUD risks
  asset_management permissions = only able to CRUD assets

  Background:
    Given a user called simon with administrator permissions and password S@feB3ar
    And a user called tom with risk_management permissions and password S@feB3ar

  @high-impact
  Scenario Outline: The administrator checks a user's details
    When simon is logged in with password S@feB3ar
    Then he is able to view <user>'s account
    Examples:
      | user |
      |tom   |

  @high-risk
  @to-do
  Scenario Outline: A user's password is reset by the administrator
    Given a <user> has lost his password
    When simon is logged in with password S@feB3ar
    Then simon can reset the <user>'s password
    Examples:
      | user |
      |tom   |
